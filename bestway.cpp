#include<iostream>
#include<vector>
#include<cstring>
#include<cstdlib>
using namespace std;

#define WIDTH 6
#define HEIGHT 6
#define MAX 99

/*
int data[WIDTH][HEIGHT]={
    0,1,1,
    0,1,0,
    0,0,0,
};
*/
int data[6][6]={
    0,1,0,0,0,1,
    0,1,0,1,0,0,
    0,1,0,1,1,0,
    0,1,0,1,1,0,
    0,1,0,0,0,0,
    0,0,0,1,1,0,
};
class Point{
public:
    int x;
    int y;
};

void printAM(int **_data, int _width, int _height)
{
    cout<<"---------------output-------------"<<endl;
    for(int i=0; i<_height; i++){
        for(int j=0; j<_width; j++){
            if(_data[i][j]==MAX) cout<<"M ";
            else cout<<_data[i][j]<<" ";
        }
        cout<<endl;
    }
    cout<<"----------------------------------"<<endl;
}

void printLen(int *_data, int size)
{
    for(int i=0; i<size; i++)
    {
        if(_data[i] == MAX) cout<<i<<":M ";
        else cout<<i<<":"<<_data[i]<<" ";
    }
    cout<<endl;
}

vector<Point> getBestWay(int **_data, int _width, int _height)
{
    int width=_width+2;
    int height=_height+2;
    int **Data = (int**)malloc(sizeof(int*)*(height));
    int i;
    for(i=0; i<height; i++){
        Data[i] = (int*)malloc(sizeof(int)*width);
    }

    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            if(i==0 || j==0 || i==height-1 || j==width-1)
                Data[i][j]=1;//add wall
            else
                Data[i][j]=_data[i-1][j-1];
        }
    }
    printAM(Data, height, width);

    int size = _width*_height;
    int **AM = (int**)malloc(sizeof(int*)*(size));
    for(i=0; i<size; i++){
        AM[i] = (int*)malloc(sizeof(int)*size);
    }

    //gen AM(Adjecent Matrix)
    for(int i=0; i<size; i++)
        for(int j=0; j<size; j++)
            AM[i][j]=MAX;
    for(int i=1; i<=_height; i++){
        for(int j=1; j<=_width; j++){
            int k=i-1;
            int m=j-1;
            if(Data[i][j] == 0 && Data[i-1][j] == 0) AM[k*_height+m][(k-1)*_height+m]=1;//up
            if(Data[i][j] == 0 && Data[i+1][j] == 0) AM[k*_height+m][(k+1)*_height+m]=1;//down
            if(Data[i][j] == 0 && Data[i][j-1] == 0) AM[k*_height+m][k*_height+m-1]=1;//left
            if(Data[i][j] == 0 && Data[i][j+1] == 0) AM[k*_height+m][k*_height+m+1]=1;//right
        }
    }

    //printAM(AM, size, size);
    int *pre = (int*)malloc(sizeof(int)*size);//the prenode of neareast
    int *visited = (int*)malloc(sizeof(int)*size);//is already visited?
    int *len = (int*)malloc(sizeof(int)*size);//neareast distense to src
    int start = 0;//top left
    int cur=start;
    int end = _height*_width-1;//bottom right
    int count=0;
    for(int i=0; i<size; i++){//initial
        pre[i]=-1;
        if(i==start) visited[i]=1;
        else visited[i]=0;
        if(i==start) len[i]=0;//self is ZERO
        else len[i]=MAX;
    }
    //printLen(len, size);
    cout<<"----------begin get best way---------"<<endl;
    while(count < size){
        bool change=false;
        for(int i=0; i<size; i++){//found all way can go through
            if(visited[i] == 1){//visit all visited node
                for(int j=0; j<size; j++){
                    if(AM[i][j]<MAX){//try every adjecent point
                        if(len[i] + AM[i][j] < len[j]){
                            len[j]=len[i]+AM[i][j];
                            visited[j] = 1;
                            pre[j]=i;
                            //cout<<"set "<<j<<":"<<len[j]<<endl;
                            //printLen(len, size);
                        }
                    }
                }
            }
        }
        if(change) break;
        count++;
    }
    cout<<"------------over-------------------"<<endl;
    cout<<"len array"<<endl;
    for(int i=0; i<_height; i++){
        for(int j=0; j<_width; j++){
            if(len[i*_width+j] != MAX) cout<<len[i*_width+j]<<" ";
            else cout<<"M ";
        }
        cout<<endl;
    }
    cout<<endl;
    //cout<<"pre";
    //printLen(pre, size);
    //cout<<"path: end["<<end<<"]";
    vector<int> tmp;
    tmp.push_back(end);
    while(true){
        if(end != start){
            //cout<<"<---"<<pre[end];
            end = pre[end];
            if(end == -1){
                cout<<"ERROR no way!!!!!"<<endl;
                break;
            }
            tmp.push_back(end);
        }else{
            cout<<endl;
            break;
        }
    }
    vector<Point> res;
    for(int i=tmp.size()-1; i>=0; i--){
        Point p;
        p.x = tmp[i]/_width;
        p.y = tmp[i]%_width;
        //cout<<"push:"<<tmp[i]<<"-["<<p.x<<","<<p.y<<"]"<<endl;
        res.push_back(p);
    }

    for(int i=0; i<res.size(); i++)
        cout<<"["<<res[i].x<<","<<res[i].y<<"]"<<"-->";
    cout<<"end"<<endl;

    free(pre);
    free(visited);
    free(len);

    //get best way

    //free
    for(i=0; i<_height; i++){
        free(AM[i]);
    }
    free(AM);

    for(i=0; i<height; i++){
        free(Data[i]);
    }
    free(Data);
    return res;
}

int main()
{
    int **AM = (int**)malloc(sizeof(int*)*(HEIGHT));
    int i;
    for(i=0; i<HEIGHT; i++)
    {
        AM[i] = (int*)malloc(sizeof(int)*WIDTH);
    }

    for(int i=0; i<HEIGHT; i++){
        for(int j=0; j<WIDTH; j++){
            AM[i][j]=data[i][j];
        }
    }

    //printAM(AM, WIDTH, HEIGHT);
    getBestWay(AM, WIDTH, HEIGHT);

    for(i=0; i<HEIGHT; i++)
    {
        free(AM[i]);
    }
    free(AM);
    return 0;
}

